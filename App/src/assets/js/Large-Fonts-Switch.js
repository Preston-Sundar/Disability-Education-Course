const largeFontSwitch = document.getElementById('largeFontSwitch');
all_elems = document.getElementById("body");
window.addEventListener('load', () => {
  if (largeFontSwitch) {
    initThemeFont();
    largeFontSwitch.addEventListener('change', () => {
      resetThemeFont();
    });
  }
});

function changeFont(element){
    if (element)
    {
      element.setAttribute("style","font-size: xx-large");
    }
      for(var i=0; i < element.children.length; i++){
          changeFont(element.children[i]);
      }
}

// function changeFontBack(element){
//     element.setAttribute("style",element.getAttribute("style")+";font-size: initial");
//     for(var i=0; i < element.children.length; i++){
//         changeFontBack(element.children[i]);
//     }
// }


// function resetFont(element){
//     element.setAttribute("style",element.getAttribute("style")+";font-size: 18");
//     for(var i=0; i < element.children.length; i++){
//         changeFont(element.children[i]);
//     }
// }


/**
 * Summary: function that adds or removes the attribute 'data-theme' depending if
 * the switch is 'on' or 'off'.
 *
 * Description: initTheme is a function that uses localStorage from JavaScript DOM,
 * to store the value of the HTML switch. If the switch was already switched to
 * 'on' it will set an HTML attribute to the body named: 'data-theme' to a 'dark'
 * value. If it is the first time opening the page, or if the switch was off the
 * 'data-theme' attribute will not be set.
 * @return {void}
 */
function initThemeFont() {
  const largeFontSelected =
    localStorage.getItem('largeFontSwitch') !== null &&
    localStorage.getItem('largeFontSwitch') === 'on';
  // largeFontSwitch.checked = largeFontSelected;
  // // largeFontSelected ? changeFont(all_elems):
  // //   changeFontBack(all_elems);
}


/**
 * Summary: resetTheme checks if the switch is 'on' or 'off' and if it is toggled
 * on it will set the HTML attribute 'data-theme' to dark so the dark-theme CSS is
 * applied.
 * @return {void}
 */
function resetThemeFont() {
  if (largeFontSwitch.checked) {
    // document.body.setAttribute('data-theme', 'dark');
    console.log("ON");
    changeFont(all_elems);
    changeFont(document.getElementById("navbarNav"));
    localStorage.setItem('largeFontSwitch', 'on');
  } else {
    // document.body.removeAttribute('data-theme');
    console.log("OFF");
    location.reload();
    // localStorage.removeItem('largeFontSwitch');
  }
}
