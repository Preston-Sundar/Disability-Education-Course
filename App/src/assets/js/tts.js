
let synth = window.speechSynthesis;
synth.rate = 0.5;
let state = 0;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

document.getElementById("stt_button").addEventListener("click", function()
{
    console.log("STT: ", state);
    stt_start();

});


document.getElementById("stt_buttonstop").addEventListener("click", function()
{
    synth.cancel();
});

async function stt_start()
{
    synth.speak(new SpeechSynthesisUtterance('Speech to text started!'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('The questions that follow can help you determine if you need help.'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('1. Lack of interest or pleasure in doing things?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('2. Feeling down, depressed, or hopeless?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('3. Feeling tired or having little energy?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('4. Feeling bad about yourself - that you let others down?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('5. Trouble concentrating on leisurely and relaxing activities?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('6. Becoming easily annoyed or irritable?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('7. Afraid of some impending failure, real or not?'));
    await sleep(5000);
    synth.speak(new SpeechSynthesisUtterance('That is the end of the survey.'));
}

