#!/bin/bash

pushd .
rm -rf static/
rm -rf templates/
mkdir static/
mkdir templates/
cd react-frontend/
npm run build
npm run backend
popd