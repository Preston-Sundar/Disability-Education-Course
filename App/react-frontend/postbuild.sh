#!/bin/bash
RED="31"
GREEN="32"
BOLDGREEN="\e[1;${GREEN}m"
ITALICRED="\e[3;${RED}m"
ENDCOLOR="\e[0m"



cp -r build/static/* ../static/
cp build/* ../static
cp build/*.html ../templates
echo -e "${BOLDGREEN}Postbuild: moved files!${ENDCOLOR}"