# Disability Educational Course (DEMO)
 
<!-- ![alt text](screen.png){height: 200px;} -->
 
<!-- <img src="https://gitlab.com/Preston-Sundar/Disability-Education-Course/-/blob/master/screen.png" height="500px" width="100%"> -->
![](screen.png)
 
Welcome!
 
--------
 
*__Target Audience:__ Dr. Celena Mondie-Milner, Executive Director, New Student Services (NSS). Kelli Bradley, Executive Director, Services for Students with Disabilities (SSD). Dr. Celena leads the team tasked with informing new students about services provided by the university. Kelli leads the SSD staff and oversees the coordination of services.*
 
This repository has all the code written to deliver this demo.
 
The purpose of this web application is to demonstrate how an online educational course regarding disabilities may look and behave. Inspiration for this project stems from the need to increase disability-related discourse amongst the student population of the University of Texas. As my previous letter to you indicates, raising awareness of disabilities is beneficial for all groups, but normalizing conversation surrounding disability-related concepts is paramount for reducing potential cases of stigmatization (Harbour and Greenberg 11). Furthermore, providing opportunities for students with disabilities to engage in meaningful and collaborative efforts bolsters student outcomes and success. In order to effectively convey and support the changes proposed in the letter, I created the following prototype web application featuring a simple course front-end layout and mid-semester anonymous self-check page.
 
The web application is a demo and primarily serves as a foundational starting piece upon which more formal student teams can build upon.
 
[Link To Demo](https://demo-cloud-7064.bss.design/index.html)
 
Further development of this application will be taken up by students from a variety of majors, encouraging collaboration, and exposing students that are inexperienced with disabilities and access to an __accessability-first__ development approach. Unfortunately, this approach to designing front-end interfaces is not often taught to developers. Considerations such as color-blind mode (supporting multiple spectrum deficiencies), high-contrast mode, ease-of-access (easy to use on a mobile device and a computer), and thoughtfully implemented text-to-speech are techniques uncommon amongst most developers. Consequently, students involved in development will be exposed to a crucial skill set required to create the accessible applications of the future.
 
Currently, this demo supports the following features,
 
1. Three distinct pages, a home page, a course page, and a survey page.
2. Implements some visual accommodation features.
3. Supports text-to-speech narration of the survey modules.
4. All of this has been implemented with the code documented. This should provide a good foundation to build from.
 
From a technology standpoint, creating this demo took a number of steps. This involved researching best-practices in the design of a course, and enhancing those practices to allow for accessibility related features to be weaved into the design. Second, the specific technologies that drive the website are Python (a programming language favoured for its ease-of-use), and React (a front-end web technology used to create scalable web applications). When a user interacts with the website, the front-end (React) parses and sends a request to the backend. The backend (Python program) unpacks this request, determines what data needs to be processed, and returns the resulting data back to the front-end to be displayed.
 
Alongside the basics of a web application, the following accessibility-oriented features are also implemented,
 
1. A high-contrast mode and switches some element's text colors and background colors to be more visually appealing (works in the course and survey page).
2. A large text setting that increases the text size, and removes visual clutter from the pages.
3. Text-to-speech within each survey question.
 
In conclusing, I hope the presentation of this prototype reinforces the need of starting a more well-supported and university-wide project.
 
 
### Works Cited
 
Harbour, Wendy S., and Daniel Greenberg. “Campus Climate and Students with Disabilities.” Association on Higher Education and Disability, vol. 1, no. 2, 2017, p. 27.
 
Mental Health America. “Take a Mental Health Test.” https://screening.mhanational.org/screening-tools/. Accessed 2021.
 
Mozilla. “Using the Web Speech API.” https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API/Using_the_Web_Speech_API. Accessed 2021.
 
New Student Services: Mission. “Mission and Vision.” Mission and Vision | New Student Services, 2020, https://orientation.utexas.edu/about/mission-and-vision. Accessed 2021.
 
SSD: Our Services. “Our Services.” 2016, https://diversity.utexas.edu/disability/our-services/.
